import Vue from 'vue'
import App from './App'
import router from './router'
//引入公共样式reset
import '../static/reset.css'
import './assets/font/iconfont.css'
//引入vuetify
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify'
Vue.use(Vuetify)
//引入修改vuetify
import './common/css/vuetify.less'
//按需引入element-ui
// import { Message } from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css';
// Vue.prototype.$message = Message;
//引入全局函数$global
import global from './utils/global.js'
Vue.prototype.$global = global
//封装axios
import HTTP from './http/index.js'
HTTP.config(Vue)
Vue.prototype.$HTTP = HTTP
Vue.config.productionTip = false
new Vue({
  el: '#app',
  router,
  components: {
    App
  },
  template: '<App/>'
})
