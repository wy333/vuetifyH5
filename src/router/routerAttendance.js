// 考勤情况路由
export default [{
    path: 'attendSet',
    meta: {
      head: '设置',bottomNav:'attendSet'
    },
    component: resolve => require(['@/pages/attend/attendSet.vue'], resolve),
  },
  {
    path: 'attendIndex',
    meta: {
      head: '考勤',bottomNav:'attendIndex'
    },
    component: resolve => require(['@/pages/attend/attendIndex.vue'], resolve),
  },
  {
    path: 'attendCount',
    meta: {
      head: '统计',bottomNav:'attendCount'
    },
    component: resolve => require(['@/pages/attend/attendCount.vue'], resolve),
  },
]
