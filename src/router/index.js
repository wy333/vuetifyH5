import Vue from 'vue'
import Router from 'vue-router'
import routerAttendance from './routerAttendance'
Vue.use(Router)

const router = new Router({
  routes: [{
      path: '/',
      redirect: '/login'
    },
    {
      path: '/index',
      redirect: '/overall'
    },
    {
      path: '/login',
      name: 'login',
      component: resolve => require(['@/pages/login'], resolve),
    },
    {
      path: '/overall',
      component: resolve => require(['@/components/layouts/overall.vue'], resolve),
      redirect:'overall/attendIndex',
      children: [
        ...routerAttendance
      ]
    },
    {//所有带返回按钮的
      path: '/outall',
      component: resolve => require(['@/components/layouts/outall.vue'], resolve),
      children:[
        {
          path: 'attendHandle',
          component: resolve => require(['@/pages/attend/attendHandle.vue'], resolve),
        },
        {
          path: 'attendClass',
          component: resolve => require(['@/pages/attend/attendClass.vue'], resolve),
        },
        {
          path: 'basic',
          meta: {
            head: '个人信息'
          },
          component: resolve => require(['@/pages/relf/basic.vue'], resolve),
        },
        {
          path: 'changePassword',
          meta: {
            head: '修改密码'
          },
          component: resolve => require(['@/pages/relf/changePassword.vue'], resolve),
        },
      ]
    },
    {
      path: '/test',
      component: resolve => require(['@/components/test/test'], resolve),
    },
  ],
  linkActiveClass: 'active',
})
export default router
