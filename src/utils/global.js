/* 
 * Author : yongWang
 * Date : 2018/12/01
 * Description : 全局工具函数和参数，通过this.$global.xx调用
 */
export default {
  isSimulate: true,
  /* 页面跳转类 */
  goBackPage() {
    window.history.back(-1);
  },
  backPage() {
    window.history.go(-1);
  },
  menuScrollTop() { //每次进页面滚动到开头
    document.getElementById('app').scrollTop = 0;
  },
  rightScrollTop() {
    document.getElementById('pageRight').scrollTop = 0;
  },
  /* 字符串处理类 */
  empty(str) { //字符串去除null
    if (str == null || str == undefined) {
      return ''
    } else {
      return str
    }
  },
  isEmpty(str) { //判断对象或字符串是否为空或不存在
    if (str == null || str == undefined) {
      return false
    } else {
      return true
    }
  },
  /* 浏览器本地缓存window.localStorage类
    缓存统一管理：同一个项目下面所有缓存名加上项目名，避免多项目缓存冲突
  */
  localSet(str1, str2) { //设置缓存
    window.localStorage.setItem(str1, str2)
  },
  localSetJson(str, obj) { //设置缓存对象-对象转字符串
    window.localStorage.setItem(str, JSON.stringify(obj))
  },
  localGet(str) { //获取缓存
    return window.localStorage.getItem(str)
  },
  localGetJson(obj) { //获取缓存对象-字符串转对象
    return JSON.parse(window.localStorage.getItem(obj))
  },
  /* 日志类 */
  log(str,color){
    console.log('%c'+str, 'color:'+(color||'green'));
  }
}
